Load data
Vectorization
Selecting best model
Best model validation result: 0.7954
Best model: LogisticRegression(C=3, class_weight=None, dual=False, fit_intercept=True,
                   intercept_scaling=1, l1_ratio=None, max_iter=100,
                   multi_class='auto', n_jobs=None, penalty='l2',
                   random_state=None, solver='lbfgs', tol=0.0001, verbose=0,
                   warm_start=False)
Refit model to full dataset
Prediction with model
Generate submission
